import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.HashMap;
import java.util.Map;

public class ChromedriverBug {
    public static void main(String[] args) {
        Map<String, String> mobileEmulation = new HashMap<>();
        mobileEmulation.put("deviceName", "Nexus 6P");

        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("headless", "disable-gpu");
        chromeOptions.setExperimentalOption("mobileEmulation", mobileEmulation);

        WebDriver driver = new ChromeDriver(chromeOptions);

        driver.get("http://example.com");

        WebElement element = driver.findElement(By.tagName("a"));

        element.click();

        try {
            new WebDriverWait(driver, 10).until((d) -> d.getTitle().startsWith("IANA"));
        } finally {
            driver.quit();
        }
    }
}
